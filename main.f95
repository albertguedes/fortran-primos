!
! file : main.f95
!
! description : Programa que encontra um número definido de primos 
!               com algoritmo otimizado.
!
! created : 2015/03/15 
! updated : 2015/03/15
! author  : albert r. carnier guedes ( albert@teko.net.br )
!

  PROGRAM primos

    ! i e j são variáveis auxiliares.
    integer*16 i,j
    
    ! Veja mais abaixo.
    integer*16 max, in_count, out_count 
    
    ! n é o número de primos que se deseja encontrar.
    integer*16 n

    ! Define o número de primos a serem mostrados.
    n = 10000

    ! Imprime o primeiro primo que é 2.
    write(*,"(I10,A,I10)"), 1 , " :: " , 2

    ! Já conta 1 e 2 como primos.
    out_count = 1

    ! Começa a procura de primos a partir do 3.
    j = 3

    do while( out_count .lt. n )

       ! Conta o número de divisores de j.
       in_count = 0

       ! Verifica se é número par, senão é descartado.
       if( modulo(j,2) .ne. 0 )then
          
          ! Calcula a raiz de j, pois o maior primo de j é menor ou igual que sua raiz.
          max = int(sqrt( real(j) ))
          do i=2,max
             
             ! Verifica se j é divisível por i.
             if( modulo(j,i) .eq. 0 )then
                ! Se for divisível, i é contado como um divisor de j.
                in_count = in_count + 1
             endif
             
          enddo
       
       else
          ! Caso j seja par, então ele é composto pelo menos de 2 como primo.
          in_count = 1
       endif

       ! Caso não tenha encontrado nenhum divisor, então j é primo.
       if( in_count .eq. 0 )then
          out_count = out_count + 1
          write(*,"(I10,A,I10)"), out_count , " :: " , j
       endif

       ! Testamos o número seguinte.
       j = j + 1

    enddo

  END PROGRAM primos
