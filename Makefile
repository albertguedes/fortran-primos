main: main.o
	gfortran -o main main.o

main.o: main.f95
	gfortran -c -o main.o main.f95

clean:
	rm main *.o *~
